<?php

    // ELGG system includes

    /***************************************************************************
    *    INSERT SYSTEM UNITS HERE
    *    You should ideally not edit this file.
    ****************************************************************************/
    
    // Plug-in engine (must be loaded first)
        require($CFG->dirroot . "units/engine/main.php");
    // Language / internationalisation
		//@todo All the libraries has a strong dependence with this 'plugin'
        require_once($CFG->dirroot . "mod/gettext/lib.php");
    // Users
        require($CFG->dirroot . "units/users/main.php");
    // Templates
        require($CFG->dirroot . "units/templates/main.php");
    // Profiles
        include($CFG->dirroot . "units/profile/main.php");
        
    // XML parsing
        require($CFG->dirroot . "lib/xmllib.php");
                
?>
