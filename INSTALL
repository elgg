Elgg version 0.8 installation instructions

Ben Werdmuller <ben@curverider.co.uk>
5 July 2007


1. BEFORE YOU BEGIN

1.1 Requirements

Please read LICENSE and README. If you're upgrading rather than making
a fresh install, also read the UPGRADE file.

Elgg currently requires the Apache web server with mod_rewrite
installed and the ability to send emails, plus PHP 4.3+ and MySQL 
4.1+ (Postgres can also be used).  It has currently been tested on 
Apache installations running Red Hat Linux and Microsoft Windows XP.

It is now possible to install Elgg on Microsoft IIS web server
environments. See the separate IIS-INSTALL file for more details, but
you need to follow the instructions below as normal.

In your Apache configuration, you must have AllowOverride set to
All for the directory where Elgg is installed.

You must have a database on the database server ready for use by Elgg.

You must have the GD library installed.

For internationalisation, you must have gettext support installed
on your server and compiled into PHP.  If this is not found, Elgg
will revert to English.

If you have any problems installing, please consider joining
http://elgg.org/ and viewing the installation support community there.

Note that if URLs like /username/weblog/ are not working for you, this
is an issue with mod_rewrite, and is an error in your Apache installation
rather than Elgg. A mod_rewrite primer (written by a third party) is
available here:

http://www.kuro5hin.org/story/2003/7/31/2335/08552#setup


1.2 Recommendations

For this release, it is recommended that MySQL has cacheing enabled.
Please see the MySQL documentation for instructions, or ask your
system administrator.


1.3 Time estimate

Assuming you have satisfied the requirements in 1.1, the installation
should not take longer than 20 minutes. This may vary depending on
the connection speed to your web server.



2. INSTALLING FILES


2.1 Deploy Elgg framework

Move all files from the distribution package to your web server root directory.
This is normally called 'public_html', 'httpdocs' or 'www'. If you like, you 
can place Elgg in a subdirectory of this; commonly, people will place it in
one called 'elgg'.

Rename htaccess-dist to .htaccess, and config-dist.php to config.php.

PLEASE NOTE: Elgg 0.8 contains integration with the Explode service at
http://ex.plode.us/. From time to time, Elgg will ping Explode to let it know
it exists. If you don't want this to happen, you must delete the folder at
/mod/explodeping.


2.2 Create data directory

The data directory needs to be written to during Elgg's normal 
operation to store uploaded files and so on.  This does not come with
Elgg out of the box, and needs to be manually created. You can 
create it anywhere reachable by the Elgg installation scripts, 
although we recommend you place it outside of your web server root.

You must assign the correct privileges.  To do this you may be able to
right click on the folder and set the "CHMOD" value, or you may have 
to use your command line terminal, navigate to the data folder, and
type:

    chmod 777 data
    
Where 'data' is the name of your data directory.


2.3 Allow admin access to the templates

If you want to use the Elgg administration panel to change templates etc,
you will also need to run the following command from the root of your Elgg
installation:

    chmod 777 mod/template/templates/Default_Template


3. SETTING UP ELGG

NB: you can ignore everything from here on in if you want to use the
visual installer. To do this, just call up the web directory you used
to install Elgg, and follow the instructions on-screen. The direct
URL for the installer is http://[your elgg install location]/_elggadmin.

3.1 Edit config.php

config.php does not exist out of the box.  You will need to copy
config-dist.php to config.php and edit the configuration values there.

There are several variables that must be set at the top of the file -
all of them are important, and they are clearly described within the
file.  Each of them is of the form:

    $CFG->data = 'value';
    
You must enter your values within the second set of quotation marks.
To include a quotation mark within a value (e.g. if you wanted 
My University's "Elgg" Trial to be your site title), prefix it with
a backslash ("\").


3.2 Customise your default homepage and other settings

Once you've filled in config.php, you can visit http://yoursite/_elggadmin/,
and log in using the main email address and news initial password. This will
allow you to change various aspects of your site, including the default
template and the site homepage.


3.3 Optional plugins

Misja Hoebe has written an XML-RPC unit that requires the PEAR library.
Please see units/rpc/README for more details. If you are sure you have
all the prerequisites for this unit, uncomment the include XMLRPC line
in the plugins section of includes.php.

We have also included an implementation of the TinyMCE editor. This
is enabled by default. To disable it, comment out the TinyMCE line in
the plugins section of includes.php.


3.4 Log in

We have included a default user, "news". This user owns all the
public custom templates. Its login name is "news" and its default
password is "password"; you should log in and change this as soon
as possible. All new users will automatically list the news account
as a friend, so you should not delete it.

The news account comes with full administrator access, whether you've
upgraded or installed fresh. To change this (we recommend that you do 
as a matter of urgency for security reasons), create a second account, 
and give that account administrator privileges using the administrator
panel.


3.5 Elgg in a subdirectory and 404 errors

If you are running Elgg in a subdirectory of your web site, and when browsing 
around it pages seem to be missing, you may also need to edit the .htaccess file. 
Below the "RewriteEngine on" line, add a line:

RewriteBase /subdirectory/

changing subdirectory to the location of Elgg relative to your overall web root.

For example, if your site is http://example.com/
              and Elgg is in http://example.com/sites/elgg/
try adding the line:

RewriteBase /sites/elgg/

If you're not running Elgg in a subdirectory on your site, but still getting lots
of 404 errors beyond the front page, you could instead try:

RewriteBase /


3.6 Tell us about it!

Because Elgg is free and open source, we often don't hear about new
installations. You don't have to, but we'd love it if you'd tell us
what you're doing with it. You can reach us at info@curverider.co.uk,
or give us a phone using the details in README.


4. FURTHER CUSTOMISATION AND DEVELOPMENTS

Please keep an eye on http://elgg.org/ for forthcoming developments 
within Elgg, including documentation on how to alter your default 
templates and writing new functionality into the system.
