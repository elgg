<?php

// ELGG VERSION INFORMATION

// This file defines the current version of the core Elgg code being used.
// This is compared against the values stored in the database to determine
// whether upgrades should be performed (see lib/db/*.php)

   $version = 2007070501;  // YYYYMMDD   = Elgg Date
                           //         X  = Elgg Point release (0,1,2...)
                           //          Y = Interim incrementer

   $release = '0.8';    // Human-friendly version name

?>
